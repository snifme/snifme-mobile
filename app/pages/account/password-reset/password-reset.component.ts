import {Component, OnInit} from 'angular2/core';
import {RouteParams,Router} from 'angular2/router';
import {FormErrorsComponent} from '../../form-errors/form-errors.component';
import {LoginLinksComponent} from '../login-links/login-links.component';
import {AjaxService} from '../../../utils/ajax.service';
import {LogService} from '../../../models/logs/log.service';
import {SessionService} from '../../../utils/session.service';

@Component({
  selector: 'password-reset-request',
  templateUrl: 'build/pages/account/password-reset/password-reset.component.html',
  directives: [LoginLinksComponent,FormErrorsComponent]
})

export class PasswordResetComponent implements OnInit {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private _ajaxService:AjaxService,
    private _routeParams:RouteParams,
    private _router:Router,
    private log: LogService,
    private _session: SessionService
  ) {}

  /*************************************
   * PROPERTIES
   *************************************/
  token:string;
  submitted:boolean = false;
  newPassword:string;
  newPasswordConfirmation:string;
  linksPage:string = "passwordReset";
  errors = {};
  showErrorMessage:boolean = false;
  sending:boolean = false;

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    this.log.pageView("/password-reset");
    this._session.redirectIfLoggedIn();
    this.token = this._routeParams.get('token');
    this.id = this._routeParams.get('id');
  }


  /*************************************
   * METHODS
   *************************************/
  validationsPass(source) {
    if (this.newPassword.length < 8) {
      const error = ["Password must have at least 8 characters."];
      source.errors.newPassword = error;
      this.log.error("Password Reset Submit", error);
      this.sending = false;
      return false;
    }
    if (this.newPassword !== this.newPasswordConfirmation) {
      const error = ["Password and Password Confirmation do not match."];
      source.errors.newPasswordConfirmation = error;
      this.log.error("Password Reset Submit", error);
      this.sending = false;
      return false;
    }
    return true;
  }

  onSubmit() {
    const _this = this;
    const log = this.log;
    log.formSubmit("Password Reset");
    this.sending = true;
    this.errors = {};
    this.showErrorMessage = false;

    if (!this.validationsPass(this)) return;

    const options = {
      data: JSON.stringify({
        newPassword: this.newPassword,
        token: this.token,
        id: this.id
      }),
      path: 'password-reset',
      action: 'PATCH'
    };

    _this._ajaxService.send(options)
    .then(() => {
      log.account("Reset", "Password");
      _this.sending = false;
      _this._router.navigate([
        'Login', { passwordReset: 'true'}
      ]);

    })
    .catch((err) => {
      _this.sending = false;
      if (err &&
          err.responseJSON &&
          err.responseJSON.errors) {
          log.error("Password Reset Submit", err.responseJSON.errors);
          _this.errors.newPassword = [err.responseJSON.errors];
      } else {
          log.error("Password Reset Submit", err);
        _this.showErrorMessage = true;
      }

    });
  }
}
