import {Alert, Modal, NavController, Page, ViewController} from 'ionic-framework/ionic';
import {FormErrorsComponent} from '../../form-errors/form-errors.component';
import {AjaxService} from '../../../utils/ajax.service';
import {LogService} from '../../../models/logs/log.service';
import {SessionService} from '../../../utils/session.service';

@Page({
  templateUrl: 'build/pages/account/password-reset-request/password-reset-request.component.html',
  directives: [FormErrorsComponent]
})

export class PasswordResetRequestComponent {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private _ajaxService:AjaxService,
    private log: LogService,
    private _session: SessionService,
    private viewCtrl: ViewController,
    private nav: NavController
  ) {}

  /*************************************
   * PROPERTIES
   *************************************/
  submitted:boolean = false;
  email:string;
  linksPage:string = "passwordResetRequest";
  errors = {};
  sending:boolean = false;

  /*************************************
   * INITIALIZER
   *************************************/
  onPageWillEnter() {
    this.log.pageView("/password-reset-email");
    this._session.redirectIfLoggedIn();
  }

  /*************************************
   * METHODS
   *************************************/
  onSubmit() {
    const _this = this;
    const log = this.log;
    log.formSubmit("Request Password Reset Email");
    this.sending = true;
    this.errors = {};

    const options = {
      data: {email: this.email, platform: "mobile"},
      path: 'password-reset-request',
      action: 'GET'
    };

    _this._ajaxService.send(options)
    .then(() => {
      _this.sending = false;
      _this.submitted = true;
      log.emailSend("Password Reset");
    })
    .catch((err) => {
      _this.sending = false;
      if (err &&
          err.responseJSON &&
          err.responseJSON.errors) {
          log.error("Password Reset Email Request", err.responseJSON.errors);
          _this.errors.email = [err.responseJSON.errors];
      } else {
          log.error("Password Reset Email Request",err);
          _this.alertError();
      }

    });
  }

  private alertError = () => {
    let alert = Alert.create({
      title: "Email Not Sent",
      subTitle: "There was a problem and we could not send the update password email. Please try again soon.",
      buttons: ["ok"]
    });
    this.nav.present(alert);
  }

  dismiss() {
     this.viewCtrl.dismiss();
   }
}
