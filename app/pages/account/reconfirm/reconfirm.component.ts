import {Alert, Modal, NavController, Page, ViewController} from 'ionic-framework/ionic';
import {FormErrorsComponent} from '../../form-errors/form-errors.component';
import {AjaxService} from '../../../utils/ajax.service';
import {LogService} from '../../../models/logs/log.service';
import {SessionService} from '../../../utils/session.service';

@Page({
  selector: 'reconfirm-account',
  templateUrl: 'build/pages/account/reconfirm/reconfirm.component.html',
  directives: [FormErrorsComponent]
})

export class ReconfirmComponent {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private _ajaxService:AjaxService,
    private log: LogService,
    private _session: SessionService,
    private nav: NavController,
    private viewCtrl: ViewController
  ) {}

  /*************************************
   * PROPERTIES
   *************************************/
  submitted:boolean = false;
  email:string;
  linksPage:string = "reconfirm";
  errors = {};
  sending:boolean = false;

  /*************************************
   * INITIALIZER
   *************************************/
  onPageWillEnter() {
    this.log.pageView("/reconfirm-account");
    this._session.redirectIfLoggedIn();
  }

  /*************************************
   * METHODS
   *************************************/
  onSubmit() {
    const _this = this;
    const log = this.log;
    log.formSubmit("Resend Confirmation");
    this.sending = true;
    this.errors = {};
    this.showErrorMessage = false;

    const options = {
      data: {email: this.email},
      path: 'resend-confirmation',
      action: 'GET'
    };

    _this._ajaxService.send(options)
    .then(() => {
      _this.sending = false;
      _this.submitted = true;
      log.emailSend("Account Confirmation");
    })
    .catch((err) => {
      _this.sending = false;
      if (err &&
          err.responseJSON &&
          err.responseJSON.errors) {
        _this.errors.email = [err.responseJSON.errors];
        log.error("Resend Confirmation Email", err.responseJSON.errors);
      } else {
        log.error("Resend Confirmation Email", err);
        _this.alertError();
      }
    });
  }

  private alertError = () => {
    let alert = Alert.create({
      title: "Confirmation Email Not Sent",
      subTitle: "There was a problem and we could not resend confirmation email. Please try again soon.",
      buttons: ["ok"]
    });
    this.nav.present(alert);
  }

  dismiss() {
     this.viewCtrl.dismiss();
   }

}
