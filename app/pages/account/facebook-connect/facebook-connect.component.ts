import {NavController,NavParams,Alert,IONIC_DIRECTIVES} from 'ionic/ionic';
import {Component} from 'angular2/core';
import {HomeComponent} from '../../secure/home/home.component';
import {LogService} from '../../../models/logs/log.service';
import {AjaxService} from '../../../utils/ajax.service';
import {SessionService} from '../../../utils/session.service';
import {FacebookService} from '../../../utils/facebook.service';

@Component({
  selector: 'facebook-connect',
  templateUrl: 'build/pages/account/facebook-connect/facebook-connect.component.html',
  directives: [IONIC_DIRECTIVES]
})

export class FacebookConnectComponent {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private _ajaxService:AjaxService,
    private nav: NavController,
    private log: LogService,
    private _session: SessionService,
    private _facebook: FacebookService
  ){}

  /*************************************
   * PROPERTIES
   *************************************/
  errors = {};
  sending: boolean = false;

  /*************************************
   * METHODS
   *************************************/

  private handleFail = (err) => {
    this.sending = false;
    this.log.error("Connect with Facebook Fail", err);
    this.alertFail();
  }

  private alertFail = () => {
    let alert = Alert.create({
      title: "Facebook Login Fail",
      subTitle: "Please try again soon",
      buttons: ["ok"]
    });
    this.nav.present(alert);
  }

  private handleNewUser = (response) => {
    localStorage.session = JSON.stringify(response);
    this.sending = false;
    if (response["user_account_create"]) {
      this.log.account("Register", "With Facebook");
      const params = {register: "true"};
      this.log.register(response.user_id);
    } else {
      const params = {login: "true"};
    }
    this.log.account("Login", "With Facebook");
    this.log.identify();
    $('#session-check').trigger('click');
    this.nav.setRoot(HomeComponent, params);
  }

  private sendToAPI = (response) => {
    const options = {
       data: JSON.stringify(response),
       path: 'facebook-connect',
       action: 'POST'
     };
    return this._ajaxService.send(options);
  }


  public facebookLogin() {
    const log = this.log;
    this.sending = true;
    this.errors = {};
    this.showErrorMessage = false;
    this.showFacebookErrorMessage = false;
    this._facebook.connect()
    .then(this.sendToAPI)
    .then(this.handleNewUser)
    .catch(this.handleFail);
  }

}
