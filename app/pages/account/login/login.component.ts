import {Page,NavController,NavParams,Alert} from 'ionic/ionic';
import {User}             from '../../../models/users/user';
import {FormErrorsComponent} from '../../form-errors/form-errors.component';
import {HomeComponent} from '../../secure/home/home.component';
import {LoginLinksComponent} from '../login-links/login-links.component';
import {RegisterComponent} from '../register/register.component';
import {FacebookConnectComponent} from '../facebook-connect/facebook-connect.component';
import {LogService} from '../../../models/logs/log.service';
import {AjaxService} from '../../../utils/ajax.service';
import {SessionService} from '../../../utils/session.service';
import {FacebookService} from '../../../utils/facebook.service';

@Page({
  selector: 'login-form',
  templateUrl: 'build/pages/account/login/login.component.html',
  directives: [
    LoginLinksComponent,
    FormErrorsComponent,
    FacebookConnectComponent
  ]
})

export class LoginComponent {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private navParams:NavParams,
    private _ajaxService:AjaxService,
    private log: LogService,
    private nav: NavController,
    private _session: SessionService
  ){}

  /*************************************
   * PROPERTIES
   *************************************/
  checkedParams:boolean = false;
  email:string;
  password:string;
  errors = {};
  submitted = false;
  linksPage: string = 'login';
  sending: boolean = false;

  /*************************************
   * INITIALIZER
   *************************************/
  onPageWillEnter() {
    this.log.pageView("/login");
    this._session.redirectIfLoggedIn();
  }

  onClickRegister() {
    this.log.linkClick('Login-Links Register');
    this.nav.setRoot(RegisterComponent);
  }

  onPageDidEnter() {
    const self = this;
    if (!self.checkedParams) {
      setTimeout(function() {
        self.checkedParams = true;
        self.checkAccountConfirmation();
        self.checkPasswordReset();
        self.checkLogout();
        self.checkUnauthorized();
      }, 50);
    }
  }

  /*************************************
   * METHODS
   *************************************/

  onSubmit() {
    const _this = this;
    const log = this.log;
    log.formSubmit("Login");
    this.sending = true;
    this.errors = {};
    this.showErrorMessage = false;

    if (this.password.length < 8) {
      const error = ["Password must be at least 8 characters"];
      this.sending = false;
      log.error("Login with Email and Password Fail",error);
      return this.errors.password = error;
    }

    const options = {
      data: JSON.stringify({
        email: this.email,
        password: this.password
      }),
      path: 'sessions',
      action: 'POST'
    };

    _this._ajaxService.send(options)
    .then((response) => {
      localStorage.session = JSON.stringify(response);
      _this.sending = false;
      _this.submitted = true;
      log.account("Login", "With Email and Password");
      _this.log.identify();
      $('#session-check').trigger('click');
      _this.nav.setRoot(HomeComponent, {login: "true"}]);
    })
    .catch((err) => {
      _this.sending = false;
      if (err &&
          err.responseJSON &&
          err.responseJSON.errors) {
        _this.errors.email = [err.responseJSON.errors];
        log.error("Login with Email and Password Fail", err.responseJSON.errors);
      } else {
        log.error("Login with Email and Password Fail", err);
        this.showErrorAlert();
      }

    });
  }

  checkAccountConfirmation() {
    let confirmed = this.navParams.get('confirmed');
    if (confirmed && confirmed === "true") {
      this.log.account("Confirm Account", "through email");
      let alert = Alert.create({
        title: "Confirmation Successful",
        subTitle: "You may now login to your account.",
        buttons: ["ok"]
      });
      this.nav.present(alert);
    } else if (confirmed && confirmed === "false") {
      this.log.error("Account Confirm", "Fail");
      let alert = Alert.create({
        title: "Confirmation Failed",
        subTitle: "You have already confirmed or your confirmation " +
                  "token is no longer valid",
        buttons: ["ok"]
      });
      this.nav.present(alert);
    }
  }

  checkPasswordReset() {
    let passwordReset = this.navParams.get('passwordReset');
    if (passwordReset) {
      let alert = Alert.create({
        title: "Password Reset",
        subTitle: "You may now login to your account using your new password",
        buttons: ["ok"]
      });
      this.nav.present(alert);
    }
  }

  checkLogout() {
    let logout = this.navParams.get('logout');
    if (logout && logout === "true") {
      let alert = Alert.create({
        title: "You are now logged out",
        subTitle: "Hope to see you back soon",
        buttons: ["ok"]
      });
      this.nav.present(alert);
    }
  }

  checkUnauthorized() {
    if (this.navParams.get('unauthorized')) {
      let alert = Alert.create({
        title: "Unauthorized",
        subTitle: "Please login first.",
        buttons: ["ok"]
      });
      this.nav.present(alert);
    }
  }

  private showErrorAlert() {
    let alert = Alert.create({
      title: "Login Failed",
      subTitle: "There was an error logging in. Please " +
                "try again soon.",
      buttons: ["ok"]
    });
    this.nav.present(alert);
  }

}
