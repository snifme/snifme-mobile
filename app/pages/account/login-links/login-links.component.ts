import {Modal, NavController,IONIC_DIRECTIVES} from 'ionic/ionic';
import {Component, OnInit,EventEmitter} from 'angular2/core';
import {LogService} from '../../../models/logs/log.service';
import {PasswordResetRequestComponent} from '../password-reset-request/password-reset-request.component';
import {ReconfirmComponent} from '../reconfirm/reconfirm.component';

@Component({
  selector: 'login-links',
  templateUrl: 'build/pages/account/login-links/login-links.component.html',
  directives: [IONIC_DIRECTIVES,PasswordResetRequestComponent,
               ReconfirmComponent],
  inputs: ['page'],
  outputs: ['clickLogin','clickRegister']
})

export class LoginLinksComponent implements OnInit {

  /*************************************
   * PROPERTIES
   *************************************/
  clickLogin: EventEmitter;
  clickRegister: EventEmitter;
  page: string;
  showRegister: boolean = false;
  showLogin: boolean = false;
  showCancel: boolean = false;
  showReconfirm: boolean = false;
  showPassword: boolean = false;

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private log: LogService,
    private nav: NavController
  ) {
    this.clickLogin = new EventEmitter();
    this.clickRegister = new EventEmitter();
  }

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    this.checkRegister();
    this.checkLogin();
    this.checkCancel();
    this.checkReconfirm();
    this.checkPassword();
  }

  /*************************************
   * METHODS
   *************************************/
  checkRegister(){
    if (this.page !== 'register') {
      this.showRegister = true;
    }
  }

  checkLogin(){
    if (this.page !== 'login') {
      this.showLogin = true;
    }

  }

  checkCancel(){

  }

  checkReconfirm(){
    if (this.page !== 'reconfirm') {
      this.showReconfirm = true;
    }
  }

  checkPassword(){
    if (this.page !== 'passwordResetRequest') {
      this.showPassword = true;
    }
  }

  clickCancel() {
    this.log.linkClick('Login-Links Cancel');
  }

  clickReconfirm() {
    this.log.linkClick('Login-Links Reconfirm');
    let modal = Modal.create(ReconfirmComponent);
    this.nav.present(modal);
  }

  clickPassword() {
    this.log.linkClick('Login-Links Password Reset');
    let modal = Modal.create(PasswordResetRequestComponent);
    this.nav.present(modal);
  }

}
