import {Page,NavController,Alert} from 'ionic-framework/ionic';
import {User}             from '../../../models/users/user';
import {FormErrorsComponent} from '../../form-errors/form-errors.component';
import {LoginLinksComponent} from '../login-links/login-links.component';
import {LoginComponent} from '../login/login.component';
import {FacebookConnectComponent} from '../facebook-connect/facebook-connect.component';
import {AjaxService} from '../../../utils/ajax.service';
import {LogService} from '../../../models/logs/log.service';
import {SessionService} from '../../../utils/session.service';

@Page({
  selector: 'register-form',
  templateUrl: 'build/pages/account/register/register.component.html',
  directives: [
    LoginLinksComponent,
    FormErrorsComponent,
    FacebookConnectComponent
  ]
})

export class RegisterComponent {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private log: LogService,
    private _ajaxService:AjaxService,
    private _session: SessionService,
    private nav: NavController
  ) {}

  // properties

  linksPage:string = "register";
  model = new User();
  sending:boolean = false;
  errors = {};
  submitted = false;

  /*************************************
   * INITIALIZER
   *************************************/
  onPageWillEnter() {
    this.log.pageView("/register");
    this._session.redirectIfLoggedIn();
  }

  onClickLogin() {
    this.log.linkClick('Login-Links Login');
    this.nav.setRoot(LoginComponent);
  }

  private handleErrors = (err) => {
    this.sending = false;
    if (err &&
        err.responseJSON &&
        err.responseJSON.errors &&
        typeof err.responseJSON.errors === 'object' &&
        Object.keys(err.responseJSON.errors).length > 0) {

      const errors = err.responseJSON.errors;
      this.log.error("Registration Fail", errors);

      for (let name in errors) {
        if (errors[name][0] == "has already been taken") {
          errors[name] = [
            "Email has already been taken." +
            " If you previously registered with Facebook, " +
            "please use the \"Connect with Facebook\" button."
          ];
        }
        this.errors[name] = errors[name];
      }
    } else {
      this.log.error("Registration Fail", err);
      this.showErrorAlert();
    }
  }

  private handleNewUser = (newUser) => {
    this.log.account("Register", "Email and Password");
    this.log.register(newUser.user.id);
    this.log.profileSet(newUser);
    this.sending = false;
    this.submitted = true;
  }


  private validateEmail = (email) => {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
  }

  private validate = () => {
    const emailValid = this.validateEmail(this.model.email);
    if (!emailValid) {
      this.errors["email"] = ["Please enter a valid email address"];
      this.log.error("Registration Fail", this.errors["email"]);
      return false;
    } else {
      return true;
    }
  }

  public onSubmit() {
    const _this = this;
    if(!this.validate() return;
    this.log.formSubmit("Register");
    this.sending = true;
    this.errors = {};
    this.showErrorMessage = false;
    this.model.save()
    .then(this.handleNewUser)
    .catch(this.handleErrors);
  }

  private showErrorAlert() {
    let alert = Alert.create({
      title: "Registration Failed",
      subTitle: "There was an error and account could not be created. Please " +
                "try again soon.",
      buttons: ["ok"]
    });
    this.nav.present(alert);
  }

}
