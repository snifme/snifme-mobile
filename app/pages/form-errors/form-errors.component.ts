import {Component} from 'angular2/core';

@Component({
  selector: 'form-errors',
  templateUrl: 'build/pages/form-errors/form-errors.component.html',
  inputs: ['errors','name']
})

export class FormErrorsComponent {

  // properties
  errors: string[];
  name: string;
}
