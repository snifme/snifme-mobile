import {Component,OnInit} from 'angular2/core';
import {RouteParams,Router} from 'angular2/router';
import {LogService} from '../../../models/logs/log.service';
import {SessionService} from '../../../utils/session.service';

@Component({
  selector: 'user-settings',
  templateUrl: 'app/pages/secure/settings/settings.component.html'
})

export class SettingsComponent implements OnInit {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private log: LogService,
    private _routeParams: RouteParams,
    private _session: SessionService,
    private _router: Router
  ) {}

  /*************************************
   * PROPERTIES
   *************************************/

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    if (window.loggingOut !== true) {
      this.log.pageView("/settings");
      this._session.redirectIfNotLoggedIn();
    } else {
      window.loggingOut = false;
      this._router.navigate(["Login", {logout: "true"}]);
    }
  }

}
