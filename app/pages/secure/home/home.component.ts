import {Page, NavController, Modal} from 'ionic/ionic';
import {LogService} from '../../../models/logs/log.service';
import {SessionService} from '../../../utils/session.service';
import {NewDogOwnerPage} from '../account/dog-owner/new/new-dog-owner';

@Page({
  selector: 'user-home',
  templateUrl: 'build/pages/secure/home/home.component.html',
  directives: [
    NewDogOwnerPage
  ]
})

export class HomeComponent {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private log: LogService,
    private _session: SessionService,
    private nav: NavController
  ) {}

  /*************************************
   * PROPERTIES
   *************************************/
  loginSuccess = false;
  registrationSuccess = false;

  /*************************************
   * INITIALIZER
   *************************************/
  onPageWillEnter() {
    if (window.loggingOut !== true) {
      this.log.pageView("/home");
      this._session.redirectIfNotLoggedIn();
      this.addFlashIfParam('register', 'registrationSuccess');
      this.addFlashIfParam('login', 'loginSuccess');
    } else {
      window.loggingOut = false;
      this.nav.setRoot(LoginComponent, {logout: "true"});
    }
  }

  addFlashIfParam(param, flashProperty) {
    const _this = this;
    let property = this._routeParams.get(param);
    if (property && property === "true" || property === true) {
      this[flashProperty] = true;
      setTimeout(() => { _this[flashProperty] = false; }, 5000);
    }
  }

  clickNewDogOwner() {
    this.log.linkClick('Home Page New Dog Account');
    let modal = Modal.create(NewDogOwnerPage);
    this.nav.present(modal);
  }

}
