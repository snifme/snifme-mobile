import {App, IonicApp, Platform} from 'ionic-framework/ionic';

// https://angular.io/docs/ts/latest/api/core/Type-interface.html
import {Type} from 'angular2/core';

// CONFIG
import {EnvService} from './config/env.service';
import {ConfigService} from './config/config.service';

// UTILS
import {AjaxService} from './utils/ajax.service';
import {FacebookService} from './utils/facebook.service';
import {SessionService} from './utils/session.service';
import {CurrentUserService} from './utils/current-user.service';
import {GeocodeService} from './utils/geocode.service';

// MODELS
import {User} from './models/users/user';
import {LogService} from './models/logs/log.service';

// PAGES
import {IndexComponent} from './index.component';
import {RegisterComponent} from './pages/account/register/register.component';
import {LoginComponent} from './pages/account/login/login.component';
import {ReconfirmComponent} from './pages/account/reconfirm/reconfirm.component';
import {PasswordResetRequestComponent} from './pages/account/password-reset-request/password-reset-request.component';
import {PasswordResetComponent} from './pages/account/password-reset/password-reset.component';
import {HomeComponent} from './pages/secure/home/home.component';
import {SettingsComponent} from './pages/secure/settings/settings.component';
import {FacebookConnectComponent} from './pages/account/facebook-connect/facebook-connect.component';
import {HelloIonicPage} from './pages/hello-ionic/hello-ionic';
import {ListPage} from './pages/list/list';

@App({
  templateUrl: 'build/app.html',
  config: {}, // http://ionicframework.com/docs/v2/api/config/Config/,
  providers: [
    AjaxService,LogService,SessionService,
    ConfigService,EnvService,FacebookService,
    FacebookConnectComponent,CurrentUserService,
    GeocodeService
  ]
})

class MyApp {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private app: IonicApp,
    private platform: Platform,
    private log: LogService,
    private _session: SessionService,
    private _facebook: FacebookService,
    private _config: ConfigService,
    private geocode: GeocodeService
  ) {

    this.initializeApp();
  }

  /*************************************
   * PROPERTIES
   *************************************/
  // make HelloIonicPage the root (or first) page
  rootPage: Type = HelloIonicPage;
  pages: Array<{title: string, component: Type}>;
  title: string = "Snifme";
  signedIn: boolean = false;

  /*************************************
   * METHODS
   *************************************/
  initializeApp() {
    const _this = this;
    // update platform plugins
    this.updatePlatform();
    // initialize mixpanel
    mixpanel.init(_this._config.ENV.MIXPANEL_KEY);
    // initialize google analytics
    this.initializeGA();
    // check session
    this.checkSession();
    // watch session changes
    this.watchSession();
    // initialize pages
    this.initializePages();
    // update log Session
    this.log.updateLogSessionToken();
    // initialize google maps
    this.geocode.initialize();
  }

  private updatePlatform = () => {
    this.platform.ready().then(() => {
    });
  }

  private watchSession = () => {
    const self = this;
    $('#session-check').on('click', self.initializePages);
  }

  private initializePages = () => {
    this.checkSession();
    // set our app's pages
    if (this.signedIn) {
      this.rootPage = HomeComponent;
      this.pages = [
        { title: 'Hello Ionic', component: HelloIonicPage },
        { title: 'My First List', component: ListPage },
        { title: 'Logout' }
      ];
    } else {
      this.rootPage = RegisterComponent;
      this.pages = [
        { title: "Register", component: RegisterComponent },
        { title: "Login", component: LoginComponent }
      ]
    }

  }

  private initializeGA = () => {
    const id = this._config.getUserId();
    if (id) {
      ga('create', this._config.ENV.GA_KEY, 'auto', {userId: id});
    } else {
      ga('create', this._config.ENV.GA_KEY, 'auto');
    }

  }

  private checkSession = () => {
    if (this._session.signedIn()) {
      window.signedIn = true;
      this.signedIn = true;
    } else {
      window.signedIn = false;
      this.signedIn = false;
    }
  }

  logout() {
    this.log.linkClick('Nav Logout');
    this._session.logOut(LoginComponent);
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.app.getComponent('leftMenu').close();

    // check for logout
    if (page.title === "Logout") {
      this.logout();
      return;
    }

    // navigate to the new page if it is not the current page
    let nav = this.app.getComponent('nav');
    nav.setRoot(page.component);
  }
}
