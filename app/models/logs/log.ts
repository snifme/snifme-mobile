import {Base} from '../base';

export class Log extends Base {

  constructor(data) {
    this.keys = [
      "name",
      "category",
      "action",
      "label",
      "session_token",
      "description",
      "user_id",
      "error_name",
      "platform",
      "device_info",
      "languages",
      "language"
    ];
    super(data,this);
  }
}
