import {Injectable} from 'angular2/core';
import {LogService} from '../models/logs/log.service';
import {ConfigService} from '../config/config.service';

@Injectable()

export class FacebookService {

  /*************************************
   * PROPERTIES
   *************************************/
  facebookInfo = [
    'age_range',
    'about',
    'bio',
    'birthday',
    'context',
    'devices',
    'education',
    'email',
    'first_name',
    'gender',
    'hometown',
    'interested_in',
    'is_verified',
    'languages',
    'last_name',
    'link',
    'location',
    'middle_name',
    'name_format',
    'political',
    'relationship_status',
    'religion',
    'significant_other',
    'timezone',
    //'token_for_business',
    'updated_time',
    'verified',
    'website',
    'work',
    'cover',
    'public_key',
    'picture'
  ].join(',');

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private log: LogService,
    private _config: ConfigService
  ){}

  /*************************************
   * METHODS
   *************************************/
  connect() {
    const self = this;
    return this.isConnected()
    .then((token) => {
      if (token) {
         return self.fetchData(token);
      } else {
        return self.loginAndFetchData();
      }
    }
  }

  private loginAndFetchData = () => {
    const self = this;
    return new Promise((resolve,reject) => {
      facebookConnectPlugin.login(
        ['email', 'public_profile'],
        (success) => {
          self.isConnected()
          .then((token) => {
            if (token) {
              self.fetchData(token)
              .then((data) {
                return resolve(data);
              })
              .catch((err) => {
                return reject(err);
              });
            } else {
              return reject("User did not login");
            }
          });
        }, (err) => {
          if (err.errorType === "OAuthException") {
            self.fetchData()
            .then((data) {
              return resolve(data);
            })
            .catch((err) => {
              return reject(err);
            });
          } else {
            return reject(err);
          }
        }
    });
  }

  private isConnected = () => {
    const self = this;
    return new Promise((resolve,reject) => {
      facebookConnectPlugin.getLoginStatus((response) => {
          return resolve(self.checkStatus(response));
        }, (fail) => {
          return reject(fail);
        });
    });
  })

  private checkStatus = (state) => {
    if (state && state.status &&
        state.status === "connected") {
      return state.authResponse.accessToken;
    } else {
      return false;
    }
  }

  private fetchData = (accessToken) => {
    const self = this;
    const token = accessToken;
    const api_params = `/me?fields=${this.facebookInfo}&access_token=${token}`;
    return new Promise((resolve,reject) => {
      facebookConnectPlugin.api(api_params, null,
        (response) => {
          response.token = token;
          return resolve(response);
        }, (err) => {
          if (err.errorType === "OAuthException") {
            self.loginAndFetchData()
            .then((data) => {
              return resolve(data);
            })
            .catch((err) => {
              return reject(err);
            });
          } else {
            return reject(err);
          }
        });
    });
  }
}
