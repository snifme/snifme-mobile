import {Injectable} from 'angular2/core';
import {ConfigService} from '../config/config.service';

@Injectable()

export class GeocodeService {

  /*************************************
   * PROPERTIES
   *************************************/
  lat:string;
  lng:string; geocodedNumber:string;
  geocodedStreet:string;
  geocodedCity:string;
  geocodedState:string;
  geocodedStateLong:string;
  geocodedCountry:string;
  geocodedCountryLong:string;
  geocodedZipcode:string;
  parsedData;

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
      private _config: ConfigService
  ) {}

  /*************************************
   * METHODS
   *************************************/
  initialize() {
    const key = this._config.ENV.GOOGLE_MAP_KEY;
    $.ajax({
       url: `https://maps.googleapis.com/maps/api/js?v=3&key=${key}`,
       dataType: "script",
       timeout:8000,
       error: (err) => {
         console.log("google maps couldn't load: " + err);
       }
    });

  }

  private prepareData = () => {
    this.parsedData = {
      latitude: this.lat,
      longitude: this.lng,
      geocodedNumber: this.geocodedNumber,
      geocodedStreet: this.geocodedStreet,
      geocodedCity: this.geocodedCity,
      geocodedState: this.geocodedState,
      geocodedStateShort: this.geocodedStateShort,
      geocodedCountry: this.geocodedCountry,
      geocodedCountryShort: this.geocodedCountryShort,
      geocodedZipcode: this.geocodedZipcode
    };
  }

  geocode(address) {
    const self = this;
    const geocoder = new google.maps.Geocoder();

    return new Promise((resolve,reject) => {
      geocoder.geocode({'address': address}, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          self.parseResults(results);
          self.prepareData();
          return resolve(self.parsedData);
        } else {
          return reject({geocodeError: status});
        }
      });
    });
  }

  private parseResults = (results) => {
    this.parseAddress(results[0].address_components);
    this.parseLatLng(results[0].geometry.location);
  }

  private parseLatLng = (loc) => {
    this.lat = loc.lat();
    this.lng = loc.lng();
  }

  private parseAddress = (components) => {
    const self = this;

    components.forEach((address_component) => {

      let name = address_component.long_name;
      let shortName = address_component.short_name;

      switch(address_component.types[0]) {
        case "route":
          self.geocodedStreet = name;
          break;
        case "locality":
          self.geocodedCity = name;
          break;
        case "country":
          self.geocodedCountry = name;
          self.geocodedCountryShort = shortName;
          break;
        case "postal_code":
          self.geocodedZipcode = name;
          break;
        case "street_number":
          self.geocodedNumber = name;
          break;
        case "administrative_area_level_1"
          self.geocodedStateShort = shortName;
          self.geocodedState = name;
          break;
      }
    }
  });
}

