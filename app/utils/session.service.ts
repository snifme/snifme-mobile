import {IonicApp} from 'ionic-framework/ionic';
import {Injectable} from 'angular2/core';
import {LogService} from '../models/logs/log.service';
import {AjaxService} from './ajax.service';

@Injectable()

export class SessionService {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private app: IonicApp,
    private log: LogService,
    private _ajaxService:AjaxService
  ){}

  redirectIfLoggedIn(HomeComponent) {
    if (this.signedIn()) {
      this.log.track('Authentication', 'Redirect', 'Trying to access logged out page when logged in')
      let nav = this.app.getComponent('nav');
      nav.setRoot(HomeComponent, {alreadyLoggedIn: "true"}]);
    }
  }

  redirectIfNotLoggedIn(LoginComponent) {
    if (!this.signedIn()) {
      this.log.track('Authentication', 'Redirect', 'Trying to access logged in page when logged out')
      let nav = this.app.getComponent('nav');
      nav.setRoot(LoginComponent, {unauthorized: "true"}]);
    }
  }


  signedIn() {
    const token = this.getToken();
    if (token && token.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  private getSession = () => {
    if(localStorage.session) {
      return JSON.parse(localStorage.session);
    }
  }

  private getToken = () => {
    const session = this.getSession();
    if (session) {
      return session.token;
    }
  }

  private destroyToken = () => {
    this._ajaxService.send({
      data: JSON.stringify({
        token: this.getToken()
      }),
      path: 'sessions-delete',
      action: 'DELETE'
    });
  }

  logOut(LoginComponent) {
    this.log.account("Logout");
    this.destroyToken();
    localStorage.session = null;
    this.log.updateLogSessionToken();
    $("#session-check").trigger('click');
    let nav = this.app.getComponent('nav');
    nav.setRoot(LoginComponent, {logout: "true"}]);
  }
}
