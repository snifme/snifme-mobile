import {Component,OnInit} from 'angular2/core';
import {ROUTER_DIRECTIVES,RouteParams} from 'angular2/router';
import {LogService} from './models/logs/log.service';
import {SessionService} from './utils/session.service';

@Component({
  selector: 'index-page',
  templateUrl: 'app/index.component.html',
  directives: [ROUTER_DIRECTIVES]
})

export class IndexComponent implements OnInit {

  /*************************************
   * PROPERTIES
   *************************************/

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private log: LogService,
    private _routeParams: RouteParams,
    private _session: SessionService
  ) {}

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    this.log.pageView("/index");
    this._session.redirectIfLoggedIn();
  }

}
