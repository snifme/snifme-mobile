import {Injectable,OnInit} from 'angular2/core';
import {EnvService} from './env.service';

@Injectable()

export class ConfigService implements OnInit {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private ENV: EnvService
  ){}

  /*************************************
   * PROPERTIES
   *************************************/
  WEB_URL:string = this.ENV.WEB_URL;
  API_URL:string = this.ENV.API_URL;

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    const environment = this.ENV.NODE_ENV || "development";

    if (environment === "development") {
      this.setupDevEnvironment();
    }

    if (environment === "test") {
      this.setupTestEnvironment();
    }

    if (environment === "staging") {
      this.setupStagingEnvironment();
    }

    if (environment === "production") {
      this.setupProductionEnvironment();
    }

  }

  public getUserId() {
    if (localStorage.session &&
      JSON.parse(localStorage.session) &&
      JSON.parse(localStorage.session)["user_id"]) {
      return JSON.parse(localStorage.session)["user_id"]
    }
    return null;
  }

  public getSessionToken() {
    return localStorage.sessionToken;
  }

}
